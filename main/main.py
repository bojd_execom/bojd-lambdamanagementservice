from database.database import init_db
from services.communication_service import CommunicationService

init_db()

communication_service = CommunicationService()

print("service started.. ")

communication_service.start()


