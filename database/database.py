from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine('sqlite:///database.sqlite', convert_unicode=True, connect_args={'check_same_thread': False})
Session = scoped_session(sessionmaker(autocommit=False,
                                      autoflush=False,
                                      bind=engine))

Base = declarative_base()
Base.query = Session.query_property()


def init_db():
    import models
    Base.metadata.create_all(bind=engine)


def get_or_create(model, **kwargs):
    session = Session()
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance


def get(model, **kwargs):
    session = Session()
    instance = session.query(model).filter_by(**kwargs).first()
    return instance


def save(model):
    session = Session()
    session.add(model)
    session.commit()


def delete(model):
    session = Session()
    session.delete(model)
    session.commit()
    session.close()
