from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from database.database import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    lambdas = relationship("LambdaModel", back_populates="owner", lazy="subquery")

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<User %r>' % self.name
