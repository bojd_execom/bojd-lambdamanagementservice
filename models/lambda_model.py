from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, REAL
from sqlalchemy.orm import relationship

from database.database import Base


class LambdaModel(Base):
    __tablename__ = 'lambdas'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    guid = Column(String(100))
    technology = Column(String(20))
    type = Column(String(20))
    trigger = Column(String(20))
    executed = Column(Integer)
    total = Column(Integer)
    average = Column(REAL(5,2))
    owner_id = Column(Integer, ForeignKey('users.name'))
    owner = relationship("User", back_populates="lambdas")

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Lambda %r>' % self.id
