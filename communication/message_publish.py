from communication.message_base import MessageBase


class MessagePublish(MessageBase):
    def __init__(self, message_object='', content='', request_id=''):
        super().__init__(message_object=message_object, content=content, request_id=request_id)
        self.predicate = 'Publish'
