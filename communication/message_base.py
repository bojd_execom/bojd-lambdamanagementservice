from abc import ABC
import zmq


class MessageBase(ABC):
    def __init__(self, predicate='', message_object='', content='', request_id=''):
        self.predicate = predicate
        self.message_object = message_object
        self.content = content
        self.request_id = request_id

    def to_protobuf_message(self):
        content = zmq.Frame()  # serialization content needs to be protobuf class instance, otherwise use empty frame
        if self.content:
            content = self.content.SerializeToString()
        return [bytes('{}.{}'.format(self.predicate, self.message_object), 'utf-8'),
                self.request_id,
                content]
