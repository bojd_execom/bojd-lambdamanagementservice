import json
import threading
from communication.service_base import ServiceBase
from errors.error_convertor import convert_service_error_to_message
from messages.error import Error
from messages.lambda_info import LambdaInfo
from messages.lambda_list import LambdaList
from messages.lambda_finished import LambdaFinished
from messages.lambda_uploaded import LambdaUploaded
from messages.message_content.LambdaFinishedMessage_pb2 import LambdaFinishedMessage
from messages.message_content.LambdasInfoMessage_pb2 import LambdasInfoMessage
from messages.message_content.LambdaMessage_pb2 import LambdaMessage
from messages.message_content.RunPostLambdaMessage_pb2 import RunPostLambdaMessage
from messages.message_content.UserMessage_pb2 import UserMessage
from services.lambda_service import LambdaService


class CommunicationService(ServiceBase):
    def __init__(self):
        super().__init__()
        self.lambda_service = LambdaService()
        self.subscribe(b'Publish.UploadLambda', self._handle_upload_lambda)
        self.subscribe(b'Publish.RunLambda', self._handle_run_lambda)
        self.subscribe(b'Get.AllLambdas', self._handle_get_all_lambdas)
        self.subscribe(b'Get.SingleLambda', self._handle_get_single_lambda)
        self.subscribe(b'Publish.EditLambda', self._handle_edit_lambda)
        self.subscribe(b'Publish.DeleteLambda', self._handle_delete_lambda)
        self.subscribe(b'Publish.RunPostLambda', self._handle_run_post_lambda)

    def start(self):
        while True:
            message = self._subscriber.recv_multipart()
            print('received message.')
            topic = message[0]
            working_thread = threading.Thread(target=self._subscriptions[topic], args=(message,))
            working_thread.start()
            print('responding..')

    def _handle_upload_lambda(self, message):
        request_message = LambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]

        guid, error = self.lambda_service.upload_lambda(request_message.FileContent,
                                                 request_message.Name,
                                                 request_message.Technology,
                                                 request_message.Type,
                                                 request_message.Trigger,
                                                 request_message.UserId)

        lambda_uploaded_message = LambdaFinishedMessage()

        if not error:
            lambda_uploaded_message.Message = guid
            lambda_uploaded = LambdaUploaded(request_id=message_id, protobuf_message=lambda_uploaded_message)
            self.send(lambda_uploaded.to_protobuf_message())
            return

        print("Error occurred while creating lambda")
        error_message = Error(request_id=message_id, protobuf_message=convert_service_error_to_message(error))
        self.send(error_message.to_protobuf_message())

    def _handle_edit_lambda(self, message):
        print("editing lambda..")
        request_message = LambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]

        guid = self.lambda_service.edit_lambda(request_message.FileContent,
                                               request_message.Id,
                                               request_message.UserId)

        lambda_edited_message = LambdaFinishedMessage()
        lambda_edited_message.Message = guid

        lambda_edited = LambdaFinished(request_id=message_id, protobuf_message=lambda_edited_message)
        self.send(lambda_edited.to_protobuf_message())

    def _handle_delete_lambda(self, message):
        print("deleting lambda..")
        request_message = LambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]

        self.lambda_service.delete_lambda(request_message.Id)

        lambda_deleted_message = LambdaFinishedMessage()
        lambda_deleted_message.Message = "Lambda deleted"

        lambda_deleted = LambdaFinished(request_id=message_id, protobuf_message=lambda_deleted_message)
        self.send(lambda_deleted.to_protobuf_message())

    def _handle_run_lambda(self, message):
        request_message = LambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]

        ret_val, error = self.lambda_service.run_lambda(request_message.Guid)
        lambda_finished_message = LambdaFinishedMessage()

        if error:
            error_message = Error(request_id=message_id, protobuf_message=convert_service_error_to_message(error))
            self.send(error_message.to_protobuf_message())
            print("Error while starting lambda")
            return

        lambda_finished_message.Message = ret_val
        lambda_finished = LambdaFinished(request_id=message_id, protobuf_message=lambda_finished_message)
        self.send(lambda_finished.to_protobuf_message())

    def _handle_run_post_lambda(self, message):
        request_message = RunPostLambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]
        ret_val, error = self.lambda_service.run_post_lambda(request_message.JsonContent , request_message.Guid)

        if error:
            print("Error occurred while running lambda")
            error_message = Error(request_id=message_id, protobuf_message=convert_service_error_to_message(error))
            self.send(error_message.to_protobuf_message())

        lambda_finished_message = LambdaFinishedMessage()
        lambda_finished_message.Message = ret_val
        lambda_finished = LambdaFinished(request_id=message_id, protobuf_message=lambda_finished_message)
        self.send(lambda_finished.to_protobuf_message())

    def _handle_get_all_lambdas(self, message):
        request_message = UserMessage()  # make user message
        request_message.ParseFromString(message[2])
        message_id = message[1]
        lambda_messages = []

        lambdas = list(self.lambda_service.get_all_lambdas(request_message.Id))

        for single_lambda in lambdas:
            lambda_message = LambdaMessage()
            lambda_message.Id = single_lambda.id
            lambda_message.Name = single_lambda.name
            lambda_message.Technology = single_lambda.technology
            lambda_message.Guid = single_lambda.guid
            lambda_messages.append(lambda_message)

        lambda_protobuf = LambdasInfoMessage()
        lambda_protobuf.Lambdas.extend(lambda_messages)

        lambda_response = LambdaList(request_id=message_id, protobuf_message=lambda_protobuf)
        self.send(lambda_response.to_protobuf_message())

    def _handle_get_single_lambda(self, message):
        request_message = LambdaMessage()
        request_message.ParseFromString(message[2])
        message_id = message[1]

        lambda_instance = self.lambda_service.get_single_lambda(request_message.Id)

        lambda_protobuf = LambdaMessage()
        lambda_protobuf.Id = lambda_instance.id
        lambda_protobuf.Name = lambda_instance.name
        lambda_protobuf.Technology = lambda_instance.technology
        lambda_protobuf.Type = lambda_instance.type
        lambda_protobuf.ImageId = lambda_instance.guid
        lambda_protobuf.Executed = lambda_instance.executed
        lambda_protobuf.Trigger = lambda_instance.trigger
        lambda_protobuf.Average = float(lambda_instance.average)
        lambda_protobuf.Total = lambda_instance.total

        lambda_response = LambdaInfo(request_id=message_id, protobuf_message=lambda_protobuf)
        self.send(lambda_response.to_protobuf_message())