import os
import shutil
import time
import zipfile
from subprocess import check_output
import subprocess
from uuid import uuid4
from database.database import get, get_or_create, save, delete
from errors.service_error import ServiceError
from models.lambda_model import LambdaModel
from models.user import User
from utils.lambda_utils import add_python_startup_script


class LambdaService:
    def __init__(self):
        self.path = "C:\\Users\\doletic\\Desktop\\PythonProjects\\bojd-lambdamanagementservice\\lambdas\\"

    def upload_lambda(self, content, name, technology, type, trigger, user_id, guid=None):
        if os.path.exists(self.path):
            print("uploading lambda..")

            owner = get_or_create(User, name=user_id)
            lambda_with_same_name = any(lambda_model.name == name for lambda_model in owner.lambdas)
            if lambda_with_same_name:
                print("Lambda with that name already exists")
                return None, ServiceError(409, "Lambda with that name already exists")

            if technology == "csharp":  # check before file is made (since csharp image isn't supported yet)
                print("Csharp isn't supported yet")
                return None, ServiceError(500, "Csharp isn't supported yet")

            if guid:
                lambda_guid_str = guid
            else:
                lambda_guid_str = str(uuid4())

            new_lambda = LambdaModel()
            new_lambda.name = name
            new_lambda.guid = lambda_guid_str
            new_lambda.technology = technology
            new_lambda.type = type
            new_lambda.trigger = trigger
            new_lambda.executed = 0
            new_lambda.average = 0
            new_lambda.total = 0
            new_lambda.owner_id = user_id

            save(new_lambda)

            new_file = open(self.path + lambda_guid_str + ".zip", "wb")
            new_file.write(content)
            new_file.close()

            self.__unzip_lambda(lambda_guid_str)

            if technology == "python":
                if trigger == "post":
                    self.__add_python_dockerfile(lambda_guid_str, entry_point='startup')
                    add_python_startup_script(self.path, lambda_guid_str)
                else:
                    self.__add_python_dockerfile(lambda_guid_str)
            else:
                return
                # self.__add_csharp_dockerfile(lambda_guid_str)

            ret_val = self.__create_image(lambda_guid_str)

            if not ret_val:
                return None, ServiceError(400, "Check if lambda convention is correct")

            print("lambda uploaded.")
            return lambda_guid_str, None

    def run_post_lambda(self, json_content, guid):
        container_name = uuid4()
        file_name = uuid4()

        self.__write_file_in_docker(json_content, file_name)

        process = subprocess.Popen(f"docker run --name {container_name} {guid.lower()}", stdout=subprocess.PIPE,
                                   shell=True)
        while True:  # continue only when docker starts container
            try:
                output = check_output(f"docker inspect -f '{{{{.State.Running}}}}' {container_name}",
                                      shell=True).decode()
                if output:
                    break
            except Exception:
                time.sleep(1)  # nothing yet, try again after 1 second
                continue
        check_output(f"docker cp ./{file_name}.json {container_name}:/init/main/content.json", shell=False).decode()

        (output, err) = process.communicate()  # await for subprocess to finish

        os.remove(f"{str(file_name)}.json")  # remove content.json file from lambda service main folder

        return output.decode(), None

    def __write_file_in_docker(self, json_content, file_name):
        json_file = open(f'{file_name}.json', 'w')
        json_file.write(str(json_content))
        json_file.close()

    def edit_lambda(self, file_content, id, user_id):
        lambda_instance = get(LambdaModel, id=id)
        self.__delete_direcetory_with_contents(lambda_instance.guid)
        delete(lambda_instance)
        check_output(f"docker rmi -f {lambda_instance.guid}", shell=True).decode()
        lambda_guid_str = self.upload_lambda(file_content,
                                             lambda_instance.name,
                                             lambda_instance.technology,
                                             lambda_instance.type,
                                             lambda_instance.trigger,
                                             user_id,
                                             guid=lambda_instance.guid)
        return lambda_guid_str

    def delete_lambda(self, id):
        lambda_instance = get(LambdaModel, id=id)
        delete(lambda_instance)
        self.__delete_direcetory_with_contents(lambda_instance.guid)

    def run_lambda(self, guid):
        new_path = self.path + guid
        if os.path.exists(new_path):
            try:
                print("Starting lambda..")
                p = check_output(f"docker run {guid.lower()}", shell=True).decode()
                print("Lambda finished.")
                return p, None
            except subprocess.CalledProcessError as ex:
                print("Lambda is not valid")
                return None, ServiceError(400, "Lambda is not valid")

        print("No lambda found")
        return None, ServiceError(400, "No lambda found")

    def get_all_lambdas(self, user_id):
        owner = get(User, name=user_id)
        if not owner:
            return []

        lambdas = owner.lambdas
        return lambdas

    def get_single_lambda(self, lambda_id):
        lambda_instance = get(LambdaModel, id=lambda_id)
        return lambda_instance

    def __unzip_lambda(self, lambda_name):
        with zipfile.ZipFile(self.path + lambda_name + ".zip", 'r') as zip_ref:
            zip_ref.extractall(self.path + lambda_name)
        self.__delete_file(lambda_name + ".zip")

    def __delete_file(self, guid):
        os.remove(self.path + guid)

    def __delete_direcetory_with_contents(self, guid):
        shutil.rmtree(self.path + guid)

    def __create_image(self, name):
        try:
            subprocess.check_output(f'docker build -t {name.lower()} .', cwd=f'{self.path + name}')
            return "Success"
        except subprocess.CalledProcessError as ex:
            self.__delete_file(name)
            print("Docker is not running")
            return

    def __add_python_dockerfile(self, lambda_name, entry_point='main'):
        docker_file = open(self.path + lambda_name + "\\Dockerfile", "w+")
        docker_file.write("FROM python:3 \n\n"
                          "ADD ./ init/ \n"
                          "WORKDIR init \n"
                          "ENV PYTHONPATH=/init \n"
                          f'ENTRYPOINT ["/usr/local/bin/python3", "main/{entry_point}.py"]')

    def __add_csharp_dockerfile(self, lambda_name):
        # todo: make dockerflie do csharp applications
        print("todo")
