def add_python_startup_script(path, lambda_name):
    startup_scritp = open(f"{path}{lambda_name}\\main\\startup.py", "w+")
    startup_scritp.write("""
import os
import time
import subprocess

FILE_NAME = 'content.json'

script_dir = os.path.dirname(__file__)
abs_file_path = os.path.join(script_dir, FILE_NAME)

while not os.path.exists(abs_file_path):
    time.sleep(1)

try:
    process = subprocess.Popen(f"python3 main/main.py", stdout=subprocess.PIPE, shell=True)
    (output, err) = process.communicate()
    print(output.decode())
except Exception:
     print("Error occurred on server.")
                            """)
