from communication.message_publish import MessagePublish


class LambdaInfo(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='LambdaInfo')
        self.content = protobuf_message
        self.request_id = request_id
