from communication.message_publish import MessagePublish


class ErrorRunLambda(MessagePublish):
    def __init__(self, request_id, protobuf_message=''):
        super().__init__(message_object='ErrorRunLambda')
        self.request_id = request_id
